/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateUnit = `subscription OnCreateUnit {
  onCreateUnit {
    id
    project_id
    code
    name
    description
    devicegroup {
      items {
        id
        name
      }
      nextToken
    }
  }
}
`;
export const onUpdateUnit = `subscription OnUpdateUnit {
  onUpdateUnit {
    id
    project_id
    code
    name
    description
    devicegroup {
      items {
        id
        name
      }
      nextToken
    }
  }
}
`;
export const onDeleteUnit = `subscription OnDeleteUnit {
  onDeleteUnit {
    id
    project_id
    code
    name
    description
    devicegroup {
      items {
        id
        name
      }
      nextToken
    }
  }
}
`;
export const onCreateDeviceGroup = `subscription OnCreateDeviceGroup {
  onCreateDeviceGroup {
    id
    name
    unit {
      id
      project_id
      code
      name
      description
      devicegroup {
        nextToken
      }
    }
    device {
      items {
        id
        code
        topic
        message
        description
      }
      nextToken
    }
  }
}
`;
export const onUpdateDeviceGroup = `subscription OnUpdateDeviceGroup {
  onUpdateDeviceGroup {
    id
    name
    unit {
      id
      project_id
      code
      name
      description
      devicegroup {
        nextToken
      }
    }
    device {
      items {
        id
        code
        topic
        message
        description
      }
      nextToken
    }
  }
}
`;
export const onDeleteDeviceGroup = `subscription OnDeleteDeviceGroup {
  onDeleteDeviceGroup {
    id
    name
    unit {
      id
      project_id
      code
      name
      description
      devicegroup {
        nextToken
      }
    }
    device {
      items {
        id
        code
        topic
        message
        description
      }
      nextToken
    }
  }
}
`;
export const onCreateDevice = `subscription OnCreateDevice {
  onCreateDevice {
    id
    code
    topic
    message
    description
    devicegroup {
      id
      name
      unit {
        id
        project_id
        code
        name
        description
      }
      device {
        nextToken
      }
    }
  }
}
`;
export const onUpdateDevice = `subscription OnUpdateDevice {
  onUpdateDevice {
    id
    code
    topic
    message
    description
    devicegroup {
      id
      name
      unit {
        id
        project_id
        code
        name
        description
      }
      device {
        nextToken
      }
    }
  }
}
`;
export const onDeleteDevice = `subscription OnDeleteDevice {
  onDeleteDevice {
    id
    code
    topic
    message
    description
    devicegroup {
      id
      name
      unit {
        id
        project_id
        code
        name
        description
      }
      device {
        nextToken
      }
    }
  }
}
`;
