/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getUnit = `query GetUnit($id: ID!) {
  getUnit(id: $id) {
    id
    project_id
    code
    name
    description
    devicegroup {
      items {
        id
        name
      }
      nextToken
    }
  }
}
`;
export const listUnits = `query ListUnits(
  $filter: ModelUnitFilterInput
  $limit: Int
  $nextToken: String
) {
  listUnits(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      project_id
      code
      name
      description
      devicegroup {
        nextToken
      }
    }
    nextToken
  }
}
`;
export const getDeviceGroup = `query GetDeviceGroup($id: ID!) {
  getDeviceGroup(id: $id) {
    id
    name
    unit {
      id
      project_id
      code
      name
      description
      devicegroup {
        nextToken
      }
    }
    device {
      items {
        id
        code
        topic
        message
        description
      }
      nextToken
    }
  }
}
`;
export const listDeviceGroups = `query ListDeviceGroups(
  $filter: ModelDeviceGroupFilterInput
  $limit: Int
  $nextToken: String
) {
  listDeviceGroups(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      name
      unit {
        id
        project_id
        code
        name
        description
      }
      device {
        nextToken
      }
    }
    nextToken
  }
}
`;
export const getDevice = `query GetDevice($id: ID!) {
  getDevice(id: $id) {
    id
    code
    topic
    message
    description
    devicegroup {
      id
      name
      unit {
        id
        project_id
        code
        name
        description
      }
      device {
        nextToken
      }
    }
  }
}
`;
export const listDevices = `query ListDevices(
  $filter: ModelDeviceFilterInput
  $limit: Int
  $nextToken: String
) {
  listDevices(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      code
      topic
      message
      description
      devicegroup {
        id
        name
      }
    }
    nextToken
  }
}
`;
