/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createUnit = `mutation CreateUnit(
  $input: CreateUnitInput!
  $condition: ModelUnitConditionInput
) {
  createUnit(input: $input, condition: $condition) {
    id
    project_id
    code
    name
    description
    devicegroup {
      items {
        id
        name
      }
      nextToken
    }
  }
}
`;
export const updateUnit = `mutation UpdateUnit(
  $input: UpdateUnitInput!
  $condition: ModelUnitConditionInput
) {
  updateUnit(input: $input, condition: $condition) {
    id
    project_id
    code
    name
    description
    devicegroup {
      items {
        id
        name
      }
      nextToken
    }
  }
}
`;
export const deleteUnit = `mutation DeleteUnit(
  $input: DeleteUnitInput!
  $condition: ModelUnitConditionInput
) {
  deleteUnit(input: $input, condition: $condition) {
    id
    project_id
    code
    name
    description
    devicegroup {
      items {
        id
        name
      }
      nextToken
    }
  }
}
`;
export const createDeviceGroup = `mutation CreateDeviceGroup(
  $input: CreateDeviceGroupInput!
  $condition: ModelDeviceGroupConditionInput
) {
  createDeviceGroup(input: $input, condition: $condition) {
    id
    name
    unit {
      id
      project_id
      code
      name
      description
      devicegroup {
        nextToken
      }
    }
    device {
      items {
        id
        code
        topic
        message
        description
      }
      nextToken
    }
  }
}
`;
export const updateDeviceGroup = `mutation UpdateDeviceGroup(
  $input: UpdateDeviceGroupInput!
  $condition: ModelDeviceGroupConditionInput
) {
  updateDeviceGroup(input: $input, condition: $condition) {
    id
    name
    unit {
      id
      project_id
      code
      name
      description
      devicegroup {
        nextToken
      }
    }
    device {
      items {
        id
        code
        topic
        message
        description
      }
      nextToken
    }
  }
}
`;
export const deleteDeviceGroup = `mutation DeleteDeviceGroup(
  $input: DeleteDeviceGroupInput!
  $condition: ModelDeviceGroupConditionInput
) {
  deleteDeviceGroup(input: $input, condition: $condition) {
    id
    name
    unit {
      id
      project_id
      code
      name
      description
      devicegroup {
        nextToken
      }
    }
    device {
      items {
        id
        code
        topic
        message
        description
      }
      nextToken
    }
  }
}
`;
export const createDevice = `mutation CreateDevice(
  $input: CreateDeviceInput!
  $condition: ModelDeviceConditionInput
) {
  createDevice(input: $input, condition: $condition) {
    id
    code
    topic
    message
    description
    devicegroup {
      id
      name
      unit {
        id
        project_id
        code
        name
        description
      }
      device {
        nextToken
      }
    }
  }
}
`;
export const updateDevice = `mutation UpdateDevice(
  $input: UpdateDeviceInput!
  $condition: ModelDeviceConditionInput
) {
  updateDevice(input: $input, condition: $condition) {
    id
    code
    topic
    message
    description
    devicegroup {
      id
      name
      unit {
        id
        project_id
        code
        name
        description
      }
      device {
        nextToken
      }
    }
  }
}
`;
export const deleteDevice = `mutation DeleteDevice(
  $input: DeleteDeviceInput!
  $condition: ModelDeviceConditionInput
) {
  deleteDevice(input: $input, condition: $condition) {
    id
    code
    topic
    message
    description
    devicegroup {
      id
      name
      unit {
        id
        project_id
        code
        name
        description
      }
      device {
        nextToken
      }
    }
  }
}
`;
